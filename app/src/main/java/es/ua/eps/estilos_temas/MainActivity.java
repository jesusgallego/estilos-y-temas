package es.ua.eps.estilos_temas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onContinuar(View v) {
        Intent intent = new Intent(this, SecondaryActivity.class);
        startActivity(intent);
    }
}
